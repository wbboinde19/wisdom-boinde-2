#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
Relatively advanced C code for CS skills course. 
This code take in the dimensions,{dimension 1, dimension 2} and 
the state of a square Sliding Tile Puzzle,{0,4,3,...n} as inputs from the terminal and 
stores information about it and prints outs its 2d state and heuristic.
To test, ./Sliding_tile_domain_attr {2,2} {2,1,0,3}.
Zero must be the least element in the state.
*/
// Create a structure of all the attributes of the puzzle
struct Puzzle {
    int nrows;
    int ncols;
    int** state;
    int Zero_row;
    int Zero_col;
    int heuristic;
};

static struct Puzzle Puzz;
int Heuristic(struct Puzzle Puzz);

int main (int argc, char *argv[]) {
/*Get the arguments from the terminal and populate the puzzle.
The arguments in the teminal when written as {a, b} {c, d} are read as
a b c d. Take them,chars and convert to ints.
*/

Puzz.nrows = atoi(argv[1])-1;
Puzz.ncols = atoi(argv[2])-1;

Puzz.state=(int**)malloc((Puzz.ncols+1)*sizeof(int)); /* Allocate mem for column */ 
for ( int i =0; i<= Puzz.ncols; i++){
    Puzz.state[i]=(int*)malloc((Puzz.nrows+1)*sizeof(int)); /* Loop through each column and allocate
    mem for the rows*/
}
//Populate the 2d Dynamic array created above.
for (int i= 3; i< argc; i++){
    int row=(i-3)/(Puzz.nrows+1);
    int col=(i-3)%(Puzz.ncols+1);
    Puzz.state[row][col]=atoi(argv[i]);
    if (atoi(argv[i])==0){
        Puzz.Zero_row=row;
        Puzz.Zero_col=col;
    }
}

Puzz.heuristic=Heuristic(Puzz);
//Print out the state of the puzzle
int columns,rows;
for(rows=0; rows <= Puzz.nrows; rows++){
    for(columns=0; columns <= Puzz.ncols; columns++){
        printf(" %d " ,Puzz.state[rows][columns]);
    }
    printf("\n");
}
printf("Heuristic is: %d\n", Puzz.heuristic);

// Free the mem
for (int i = 0; i <= Puzz.ncols; i++)
    free(Puzz.state[i]);
 
free(Puzz.state);

return 0;
}
/* populating the function */ 
int Heuristic(struct Puzzle Puzz){
    for (int i=0; i < Puzz.ncols; i++){
        for (int j=0; j < Puzz.nrows; j++){
            int tile_number= Puzz.state[i][j];
            if (tile_number==0)
                continue;
            int goal_row=tile_number/Puzz.nrows;
            int goal_column=tile_number%Puzz.nrows;
            if (goal_row == i && goal_column== j)
                ;
            else
                Puzz.heuristic+=(abs((j)-(goal_column))+abs((i)-(goal_row)));

            
        }
    }
    return Puzz.heuristic;
}




