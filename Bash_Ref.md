# Bash References
## Resources
* Markdown syntax: 'https://www.markdownguide.org/basic-syntax/'
* Working with the '.bashrc' file: 'https://towardsdatascience.com/how-to-bash-for-those-of-us-who-dont-like-the-command-line-8cbf701356dd'
* Bash Aliases: 'https://opensource.com/article/19/7/bash-aliases'
* Other: 'https://www.tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html#sect_03_02'

## Exploring the Terminal
* Simple commands: echo, pwd, ls, cd, 

## Organizing files
* Simple commands: mkdir, mv, touch, rm, rmdir, cp
* scp = cluster copying

> scp [file name] [username@cluster.earlham.edu]:     # copy to cluster from local computer
>
> scp [username@cluster.earlham.edu]:[filename] .     # copy from cluster to working directory on local computer


## Viewing files
* Simple commands: cat, less, man, 
* We can also edit by using the 'nano' command

## Pipeline and Filters
### The Pipe Operator
The pipe operator, |, sends the output of one command as the input for another

### Filters
* Simple commands: wc
* grep or global regular expression print searches for lines with a given string or looks for a pattern in a given input stream
* uniq stands for unique and gives us the number of unique lines in the input stream. Uniq cannot detect duplicates unless they are adjacent to each other. Thus you have to sort using the sort filter
* sort. This sorts lines alphabetically or numerically. If you want to combine sort and uniq, sort -u.

## Working with The '.bashrc' File
The .bashrc file lives in the root of your home directory. Most terminals and file UIs do not show "dot files" by default; they are hidden files. You can see yours by entering:

> $ ls -a ~/.bashrc
>
> /c/Users/nicho/.bashrc*

It is home base for anything that needs to run when the shell program starts.
* environment variables
* functions
* aliases
* settings
* prompt
* autocompletions

### Bash Aliases
A Bash alias is a method of supplementing or overriding Bash commands with new ones.
> alias ls='ls -F'
>
> alias ll='ls -lh'

This will only alias it locally. To make it permanent, add alias manually.
If you add aliases manually, then you must load them into your current Bash session:

> $ source ~/.bashrc

To add them manually, 
> vi ~/.bash_aliases *(and manually update it)*
>
>source ~/.bash_aliases

### Basic Functions for Getting Things Done
Bash functions have an easy enough syntax to declare:

> function_name () {
>
>   #logic here
>
> }

## The For loop
The syntax for this loop is:

> for NAME [in LIST ]; do COMMANDS; done

examples:

> for i in {1..5}; do COMMAND-HERE; done
>
> for((i=1;i<=10;i+=2)); do echo "Welcome $i times"; done
>
> for i in *; do echo $i; done *For files*


## Variables
Shell variables are in uppercase characters by convention. Bash keeps a list of two types of variables: Global and Local
* Global variables or environment variables are available in all shells. The 'env' or 'printenv' commands can be used to display environment variables. These programs come with the sh-utils package.
* Local variables are only available in the current shell. Using the 'set' built-in command without any options will display a list of all variables (including environment variables) and functions. The output will be sorted according to the current locale and displayed in a reusable format.

## Scripts
A shell script is a sequence of commands for which you have a repeated use. This sequence is typically executed by entering the name of the script on the command line. Alternatively, you can use scripts to automate tasks using the cron facility. Another use for scripts is in the UNIX boot and shutdown procedure, where operation of daemons and services are defined in init scripts.

To create a shell script, open a new empty file in your editor. Any text editor will do: vim, emacs, gedit, dtpad et cetera are all valid. You might want to chose a more advanced editor like vim or emacs, however, because these can be configured to recognize shell and Bash syntax and can be a great help in preventing those errors that beginners frequently make, such as forgetting brackets and semi-colons.

Give your script a sensible name that gives a hint about what the script does. Make sure that your script name does not conflict with existing commands. In order to ensure that no confusion can rise, script names often end in .sh; even so, there might be other scripts on your system with the same name as the one you chose. Check using which, whereis and other commands for finding information about programs and files:

> which -a script_name
>
> whereis script_name
>
> locate script_name

To excute your script, 
> chmod u+x script1.sh

# Command History:
|echo "Basic Commands"                                      |   Commands continued                       |
|-----------------------------------------------------------|--------------------------------------------|              
|cd ..                                                      |cd wisdom-boinde-2/                         |
|cat Readme.md                                              |pwd                                         |
|man ls                                                     |sudo apt install vim                        |
|ls -a                                                      |vim Binary_search.                          |
|ls -a -l                                                   |alias cd -boind="cd /mnt/c/Users/boind"     |
|echo Filters                                               |alias boind="cd /mnt/c/Users/boind"         |
|ls                                                         |boind                                       |
|cat Bash_Ref.md                                            |for i in *; do echo $i is part; done        |
|cd ..                                                      |history                                     |
|ls                                                         |cd CS_Stuff/CS_skills_2/                    |
|cd CS_310/                                                 |ls                                          |
|ls CS_310 | sort | grep "Bin"                              |cd wisdom-boinde-2/                         |
|cat Binary_search.c | wc -l                                |ls                                          |
|nano Binary_search.c                                       |history > Bash_Ref.md                       |

# Regular Expressions
# Basics Commands used with Regular Expression
Commonly used commands include: tr, sed,awk, vi, and grep.
## Working with grep
### grep command examples in Linux and Unix
Below is some standard grep command explained.

* Search any line that contains the word in filename on Linux:
> grep 'word' filename

* Perform a case-insensitive search for the word ‘bar’ in Linux and Unix:
> grep -i 'bar' file1

* Look for all files in the current directory and in all of its subdirectories in Linux for the word ‘httpd’:
> grep -R 'httpd' .

* Search and display the total number of times that the string ‘nixcraft’ appears in a file named frontpage.md:
> grep -c 'nixcraft' frontpage.md

For searching in a file, use *fgrep word filename*. To search for lines where the whole word exists, add -w to the command.

To search for 2 words, use the egrep command
> egrep -w 'word1|word2' /path/to/file

To count line, add -c. To add the line number to the output, use -n. you can also get the output in color for easy read. Just add --color.

## Working with sed
Either resads from a text file ot from standard input. It also outputs everything in standard out.
sed [options] commands [file to eit] 

### Basic Uses
* can be used as a basic text reader.
> sed '' [file to read]
You can also use standard input rather than a file.
> cat [file] | sed ''

If you want to target specific parts of a text stream, you can specify a specific line or even a range of lines.
> sed -n '1p' [file]
prints the first line to the screen. By puting a number before p, you specify the line number you want to operate. To give an address range,
> sed -n '1,5p' [file] 
This will print the first five lines of the text. It means 1 through 5

* To delete instead of print, replace the p with d and you no longer need -n.
> sed '1,5d' [file]

* It can also be used to substitute text.
> sed 's/old_word/new_word' [file]
This is only changes the first instance of the old word in each line. also, if the word is in a bigger word, it is still changed.
> sed 's/old/new/g' [file] 
Takes care of the other instances of the old word. To specify the instance you want to change, use a number in place of g.
If you wanted to see which lines were substituted, use the -n option and add p to the instance specification.
## Awk
Used for manipulating data and generating reports
> awk option 'selection_creteria {action}' [file]
*awk '{print}' [file]* prints everything in the file

To print lines which make a selection creteria, 
> awk '/pop/ {print}' [file]  
This will print the lines with pop in it.

*Splitting a line into fields
in the {print} you can specify a range {print $1,$4}. The first word is stored in $1. $0 represents the whole line.
If you want to desplay the line number 
> awk '{print NR,$0} [file]

To Learn more about awk: https://www.geeksforgeeks.org/awk-command-unixlinux-examples/
## tr
This is a command line utility for translating or deleting characters.
> tr [option] set1 set2

Link: https://www.geeksforgeeks.org/tr-command-in-unix-linux-with-examples/

# Regular expressions
Special characters which help search data and match complex patterns.
## Baic Regular expressions
| Symbol | Descriptions
|--------|-------------------------------------------------------
| .	 | replaces any character
| ^	 | matches start of string
| $	 | matches end of string
| *	 | matches up zero or more times the preceding character
| \	 | Represent special characters
| ()	 | Groups regular expressions
| ?	 | Matches up exactly one character

# Examples
'cat Test_file.txt | grep ^y': matches y start of lines and prints it out
'cat Test_file.txt | grep ^ya' : matches ya start of lines and prints it our
'cat Test_file.txt | grep ya*': Does same as above.
NB: If you want to use a regular expresion as a specila character
> cat Test_file | grep  

## Interval Regualar expressions
|expression	| Description
|---------------|-------------------------------------------------------------------------
| {n}	        | Matches the preceding character appearing ‘n’ times exactly
| {n,m}	        | Matches the preceding character appearing ‘n’ times but not more than m
| {n, }	        | Matches the preceding character only when it appears ‘n’ times or more

NB: you need to add -E as an option for these regex

'cat Test_file | egrep -iE b\{2}' : will Match and print out lines with case insensitive 2 bs

The other two {n,m} and {n,} are range variations of preceding characters

## Extended regular expressions
These tegular expressions contain comnbinations of more than one expression. 
| Expression	| Description
|---------------|--------------------------------------------------------------------------
| \+	        | Matches one or more occurrence of the previous character
| \?            | Matches zero or one occurrence of the previous character
