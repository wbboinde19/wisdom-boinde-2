# Basics Commands used with Regular Expression
Commonly used commands include: tr, sed,awk, vi, and grep.
## Working with grep
### grep command examples in Linux and Unix
Below is some standard grep command explained.

* Search any line that contains the word in filename on Linux:
> grep 'word' filename

* Perform a case-insensitive search for the word ‘bar’ in Linux and Unix:
> grep -i 'bar' file1

* Look for all files in the current directory and in all of its subdirectories in Linux for the word ‘httpd’:
> grep -R 'httpd' .

* Search and display the total number of times that the string ‘nixcraft’ appears in a file named frontpage.md:
> grep -c 'nixcraft' frontpage.md

For searching in a file, use *fgrep word filename*. To search for lines where the whole word exists, add -w to the command.

To search for 2 words, use the egrep command
> egrep -w 'word1|word2' /path/to/file

To count line, add -c. To add the line number to the output, use -n. you can also get the output in color for easy read. Just add --color.

## Working with sed
Either resads from a text file ot from standard input. It also outputs everything in standard out.
sed [options] commands [file to eit] 

### Basic Uses
* can be used as a basic text reader.
> sed '' [file to read]
You can also use standard input rather than a file.
> cat [file] | sed ''

If you want to target specific parts of a text stream, you can specify a specific line or even a range of lines.
> sed -n '1p' [file]
prints the first line to the screen. By puting a number before p, you specify the line number you want to operate. To give an address range,
> sed -n '1,5p' [file] 
This will print the first five lines of the text. It means 1 through 5

* To delete instead of print, replace the p with d and you no longer need -n.
> sed '1,5d' [file]

* It can also be used to substitute text.
> sed 's/old_word/new_word' [file]
This is only changes the first instance of the old word in each line. also, if the word is in a bigger word, it is still changed.
> sed 's/old/new/g' [file] 
Takes care of the other instances of the old word. To specify the instance you want to change, use a number in place of g.
If you wanted to see which lines were substituted, use the -n option and add p to the instance specification.
## Awk
Used for manipulating data and generating reports
> awk option 'selection_creteria {action}' [file]
*awk '{print}' [file]* prints everything in the file

To print lines which make a selection creteria, 
> awk '/pop/ {print}' [file]  
This will print the lines with pop in it.

*Splitting a line into fields
in the {print} you can specify a range {print $1,$4}. The first word is stored in $1. $0 represents the whole line.
If you want to desplay the line number 
> awk '{print NR,$0} [file]

To Learn more about awk: https://www.geeksforgeeks.org/awk-command-unixlinux-examples/
## tr
This is a command line utility for translating or deleting characters.
> tr [option] set1 set2

Link: https://www.geeksforgeeks.org/tr-command-in-unix-linux-with-examples/

# Regular expressions
Special characters which help search data and match complex patterns.
## Baic Regular expressions
| Symbol | Descriptions
|--------|-------------------------------------------------------
| .	 | replaces any character
| ^	 | matches start of string
| $	 | matches end of string
| *	 | matches up zero or more times the preceding character
| \	 | Represent special characters
| ()	 | Groups regular expressions
| ?	 | Matches up exactly one character

# Examples
'cat Test_file.txt | grep ^y': matches y start of lines and prints it out
'cat Test_file.txt | grep ^ya' : matches ya start of lines and prints it our
'cat Test_file.txt | grep ya*': Does same as above.
NB: If you want to use a regular expresion as a specila character
> cat Test_file | grep  

## Interval Regualar expressions
|expression	| Description
|---------------|-------------------------------------------------------------------------
| {n}	        | Matches the preceding character appearing ‘n’ times exactly
| {n,m}	        | Matches the preceding character appearing ‘n’ times but not more than m
| {n, }	        | Matches the preceding character only when it appears ‘n’ times or more

NB: you need to add -E as an option for these regex

'cat Test_file | egrep -iE b\{2}' : will Match and print out lines with case insensitive 2 bs

The other two {n,m} and {n,} are range variations of preceding characters

## Extended regular expressions
These tegular expressions contain comnbinations of more than one expression. 
| Expression	| Description
|---------------|--------------------------------------------------------------------------
| \+	        | Matches one or more occurrence of the previous character
| \?            | Matches zero or one occurrence of the previous character



