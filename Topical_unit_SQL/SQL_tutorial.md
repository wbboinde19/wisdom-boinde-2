# SQL Tutorial
SQL is a standard language for storing, manipulation, and retrieving data in database.

## SQL Syntax
DDL stands for Data Definition Language. It is the standard that specifies the structures in a database. They include:
create table, alter table, drop table, create index, etc.

DML stands for Data Manipulation Language. E.g select, insert, update, delete, etc.

DCL stands for Data Control Language whose syntax is used to control access to data in the database.These commands include:
* GRANT: This command gives users access privileges to the database.
* REVOKE: This command withdraws the user’s access privileges given by using the GRANT command.
### Database Tables
A database most often contains a number of tables which each identifies by a name.

### SQL Statements
Most actions on the database are done with SQL statements. As SQL statements are not case sensitive, the command 'select' is the same as 'SELECT'.
At the end of each SQL statement, the database system require a semicolon at the end.

> SELECT * FROM table_name

### SELECT - Extracts data from the database
The data returned is stored in a result table, called *result-set*
#### Syntax

> SELECT column1, column2, ....

> FROM table_name;

Example:

> SELECT * FROM mock_data;

This querry will print out all the data from the mock_data table.

As can be seen in the syntax, you select the column. The syntax above is usually writen in a line.
#### Other Select commands
* SELECT DISTINCT column FROM table_name - this displays unique elements of column. ie no repeats
* You can count by using COUNT( )

### SQL WHERE
Used to add a condition to an action.

> SELECT * FROM table_name WHERE Culumn_input=*this* ;

Operators that can be used are: =, >, <, <>-meaning not equal to, BETWEEN .. AND .. -range, LIKE-pattern, IN(posible values) - to specify multiple possible values for a column.
You can use WHERE with AND, OR, and NOT operators to filter as well. 

### SQL ORDER BY
You can specify the order

> SELECT column1, column2,....
>
> FROM table_name
> 
> ORDER BY column1, column2, ... ASC|DESC;


### UPDATE - Updates data from the database
Used to modify existing records

> UPDATE table_name
>
> SET column1 = value1, column2 = value2, ...
>
> WHERE condition;

### DELETE
used to delete existing records

> DELETE FROM table_name WHERE condition;

### INSERT INTO- Used to insert new records into the table.
To specify both the column names and the values to be inserted:

> INSERT INTO table_name (column1, column2, column3, ...)
>
> VALUES (value1, value2, value3, ...);

If you are adding for all the columns, no need to specify them in the INTO.

### SQL GROUP BY Statement
groups rows that have the same values into summary rows
> SELECT column_name(s)
>
> FROM table_name
>
> WHERE condition
>
> GROUP BY column_name(s)
>
> ORDER BY column_name(s);

The GROUP BY statement is often used with aggregate functions (COUNT(), MAX(), MIN(), SUM(), AVG()) to group the result-set by one or more columns

Example:
We can count the number of people in mock_data with same age and order them by age in descending order as follows

> SELECT COUNT(id), age FROM mock_data GROUP BY age ORDER BY age DESC;

This will produce the outcome:
 
 |count | age
 |------|-----
 |   44 | 30
 |   40 | 29
 |   38 | 28
 |   41 | 27
 |   42 | 26
 |   51 | 25
 |   50 | 24
 |   35 | 23
 |   58 | 22
 |   32 | 21
 |   39 | 20
 |   55 | 19
 |   45 | 18
 |   33 | 17
 |   45 | 16
 |   50 | 15
 |  302 |
(17 rows) 
                                                          
### SQL TOP/LIMIT 
The SELECT TOP clause is used to specify the number of records to return.

> SELECT TOP 3 * FROM Customers;

> SELECT * FROM Customers
>
> LIMIT 3; 

Example:

> SELECT COUNT(id), age FROM mock_data GROUP BY age ORDER BY age DESC LIMIT 5;

limits the outcome of the print out to 5 as shown:
 
| count | age
|-------|----
|    44 | 30
|    40 | 29
|    38 | 28
|    41 | 27
|    42 | 26
(5 rows)


### CREATE DATABASE
Used to create a new SQL database.

> CREATE DATABASE databasename;

### CREATE TABLE - creates a new table

> CREATE TABLE table_name (
> 
>   column1 datatype,
>
>   column2 datatype,
>
>   column3 datatype,
>
>   ....
>
> );

The dataypes are int, varchar(number of chars)
To create new table from an existinf table,

> CREATE TABLE new_table_name AS
>
>    SELECT column1, column2,...
>
>    FROM existing_table_name
>
>    WHERE ....;

### ALTER TABLE

> ALTER TABLE table_name
>
> ADD column_name datatype;

### DROP TABLE -  deletes a table

> DROP TABLE table_name;

### CREATE INDEX - creates and index (search key)
Indexes are used to retrieve data from the database more quickly than otherwise. The users cannot see the indexes, they are just used to speed up searches/queries

> CREATE INDEX index_name
>
> ON table_name (column1, column2, ...);

We can also drop the index using *DROP INDEX*

 
