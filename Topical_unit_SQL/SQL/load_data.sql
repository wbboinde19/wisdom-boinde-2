create table MOCK_DATA (id BIGSERIAL NOT NULL PRIMARY KEY,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	gender VARCHAR(50),
	age VARCHAR(7),
	email VARCHAR(50)
); 	
\COPY MOCK_DATA (first_name, last_name, gender, age, email) FROM '/eccs/home/wbboinde19/SQL/MOCK_DATA.csv' WITH (format csv, delimiter ',', null 'NULL', header True);
