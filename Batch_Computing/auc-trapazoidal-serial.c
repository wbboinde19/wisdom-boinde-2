/* 
  auc-trapazoidal-serial.c (based on Pacheco's version) - calculate the area under 
  a curve using a completely serial algorithm. 

  $ auc-trapazoidal-serial a b n 
  defaults: a = 2, b = 10, n = 100
  function: f(x) = x^2 + 3.5x - 5
  b must be > a
  n must be > 0
*/ 

// todo - function vector for calling function, command line option 

#include <stdio.h>
#include <math.h>
#include <getopt.h> 
#include <stdlib.h>

double theFunction(double x); 

int main(int argc, char *argv[]) { 
  long int n;
  int i, debug = 0, opt = 0;
  double a, b, h, x_i, area; 

  while ((opt = getopt(argc, argv, "da:b:n:")) != -1) { 
      switch (opt) {
          case 'd':
              debug = 1;
              break;

          case 'a':
              a = strtoul(optarg, (char**) NULL, 10);
              fprintf(stderr, "a= %f\n",a); 
              break;

          case 'b':
              b = strtoul(optarg, (char**) NULL, 10);
              fprintf(stderr, "b= %f\n",b); 
              break;

          case 'n':
              n = strtoul(optarg, (char**) NULL, 10);
              fprintf(stderr, "n= %ld\n",n); 
              break;

          case '?':
              fprintf(stderr, "usage: -d -a <range-start> -b <range-end> -n <number-slices>\n"); 
              fprintf(stderr, "where -d enables debugging\n"); 
              exit(1); 

          default:
              break; 
      } 
  }
  if (argc == 1) {
    a=2;
    b=10;
    n=100;
    fprintf(stderr, "a = %lf, b = %lf, n = %ld\n", a, b, n); 
  }
  if (argc == 2 ){
    a=2;
    b=10;
    n=100;
    debug = 1;
  }
  // Actually do some work.
  h = (b - a) / (double)n;
  area = (theFunction(a) + theFunction(b)) / (double)2.0; 

  for (i = 1; i <= (n - 1); i++) {
    x_i = a + (i * h);
    area += theFunction(x_i); }
      
  area = h * area; 
  
  if (debug) 
    fprintf(stderr, "a = %lf, b = %lf, n = %ld, area = %lf\n", a, b, n, area); 
  
  return(0); } 

double theFunction(double x) {
  return((x * x) + x * (double)3.5 - (double)5);
}
